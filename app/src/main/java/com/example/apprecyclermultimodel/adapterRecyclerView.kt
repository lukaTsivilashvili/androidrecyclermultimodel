package com.example.apprecyclermultimodel

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.apprecyclermultimodel.databinding.CpuLayoutBinding
import com.example.apprecyclermultimodel.databinding.GpuLayoutBinding
import com.example.apprecyclermultimodel.modelClasses.CpuModelClass
import com.example.apprecyclermultimodel.modelClasses.GpuModelClass

class adapterRecyclerView(
    private val context: Context,
    private val cpus: MutableList<CpuModelClass>,
    private val gpus: MutableList<GpuModelClass>,
    private val cpuListener: cpuRemover,
    private val gpuListener: gpuRemover
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_TYPE_CPU = 1
        private const val VIEW_TYPE_GPU = 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == VIEW_TYPE_CPU) {
            cpuViewHolder(
                CpuLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            gpuViewHolder(
                GpuLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is cpuViewHolder -> holder.bind()
            is gpuViewHolder -> holder.bind()
        }
    }

    override fun getItemCount(): Int {
        return cpus.size + gpus.size
    }


    inner class cpuViewHolder(private val binding: CpuLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnLongClickListener {

        private lateinit var modelCpu: CpuModelClass
        fun bind() {
            modelCpu = cpus[adapterPosition]
            binding.CpuTVmodel.text = modelCpu.model
            binding.CpuTVcores.text = modelCpu.cores.toString()
            binding.CpuTVthreads.text = modelCpu.threads.toString()
            binding.CpuTVfrequency.text = modelCpu.coreFrequency
            binding.CpuTVprice.text = modelCpu.price.toString()
            binding.CpuIV.setImageResource(R.mipmap.ic_launcher_round)
            binding.root.setOnLongClickListener(this)

            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(16))

            Glide.with(context)
                .load(modelCpu.url)
                .placeholder(R.drawable.ic_launcher_background)
                .apply(requestOptions)
                .into(binding.CpuIV)

        }

        override fun onLongClick(v: View?): Boolean {
            cpuListener.userCpuRemover(adapterPosition)
            return true
        }

    }

    inner class gpuViewHolder(private val binding: GpuLayoutBinding) :
        RecyclerView.ViewHolder(binding.root), View.OnLongClickListener {

        private lateinit var modelGpu: GpuModelClass
        fun bind() {
            modelGpu = gpus[adapterPosition - cpus.size]
            binding.GpuTVmodel.text = modelGpu.model
            binding.GpuTVmemType.text = modelGpu.memoryType
            binding.GpuTVmemory.text = modelGpu.memory
            binding.GpuTVtdp.text = modelGpu.tdp
            binding.GpuTVprice.text = modelGpu.price.toString()
            binding.GpuIV.setImageResource(R.mipmap.ic_launcher)
            binding.root.setOnLongClickListener(this)


            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(16))

            Glide.with(context)
                .load(modelGpu.url)
                .placeholder(R.drawable.ic_launcher_background)
                .apply(requestOptions)
                .into(binding.GpuIV)

        }

        override fun onLongClick(v: View?): Boolean {
            gpuListener.userGpuRemoved(adapterPosition-cpus.size)
            return true
        }
    }

    override fun getItemViewType(position: Int): Int {

        if (position > cpus.size - 1) {
            return VIEW_TYPE_GPU
        } else {
            return VIEW_TYPE_CPU
        }

    }
}