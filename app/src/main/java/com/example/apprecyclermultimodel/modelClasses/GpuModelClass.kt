package com.example.apprecyclermultimodel.modelClasses

data class GpuModelClass(
    val model:String,
    val memoryType:String,
    val memory:String,
    val tdp:String,
    val price:Int,
    val url:String?
)
