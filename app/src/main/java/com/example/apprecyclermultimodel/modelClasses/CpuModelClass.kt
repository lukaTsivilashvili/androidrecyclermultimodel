package com.example.apprecyclermultimodel.modelClasses

data class CpuModelClass(
    val model:String,
    val cores:Int,
    val threads:Int,
    val coreFrequency:String,
    val price:Int,
    val url:String?
)
