package com.example.apprecyclermultimodel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apprecyclermultimodel.databinding.ActivityMainBinding
import com.example.apprecyclermultimodel.modelClasses.CpuModelClass
import com.example.apprecyclermultimodel.modelClasses.GpuModelClass

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: adapterRecyclerView
    private val cpuList = mutableListOf<CpuModelClass>()
    private val gpuList = mutableListOf<GpuModelClass>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        addData()
        adapter = adapterRecyclerView(this, cpuList, gpuList, object : cpuRemover {
            override fun userCpuRemover(position: Int) {
                cpuList.removeAt(position)
                adapter.notifyItemRemoved(position)
            }
        }, object : gpuRemover {
            override fun userGpuRemoved(position: Int) {
                gpuList.removeAt(position)
                adapter.notifyItemRemoved(cpuList.size + position)
            }
        })
        binding.pcRecyclerView.layoutManager = LinearLayoutManager(this)
        binding.pcRecyclerView.adapter = adapter
    }

    private fun addData() {
        cpuList.add(
            CpuModelClass(
                "Intel Core I9 10900K",
                8,
                16,
                "5.1",
                499,
                "https://www.bhphotovideo.com/images/images1500x1500/intel_core_i9_9900k_3_6_ghz_1569077.jpg"
            )
        )
        cpuList.add(
            CpuModelClass(
                "Intel Core I7 10700K",
                6,
                12,
                "4.9",
                399,
                "https://www.bhphotovideo.com/images/images1500x1500/intel_bx80684i79700_core_i7_9700_processor_boxed_1469515.jpg"
            )
        )
        cpuList.add(
            CpuModelClass(
                "Intel Core I5 10400",
                4,
                8,
                "4.5",
                165,
                "https://www.powerplanetonline.com/cdnassets/procesador_intel_core_i5-9600k_01_l.jpg"
            )
        )
        cpuList.add(
            CpuModelClass(
                "Intel Core I3 10100",
                2,
                4,
                "4.2",
                115,
                "https://cdn.mos.cms.futurecdn.net/4sPVUZo6iybivzkumYbE6j.jpg"
            )
        )

        gpuList.add(
            GpuModelClass(
                "Msi Ventus RTX 3090",
                "GDDR6",
                "24GB",
                "350W",
                1499,
                "https://cdn.wccftech.com/wp-content/uploads/2020/09/MSI_GeForce-RTX%E2%84%A2-3090-VENTUS-3X-24G_BOX-125f4e65c76348d3.34594564-740x633.png"
            )
        )
        gpuList.add(
            GpuModelClass(
                "Asus ROG Strix RTX 3080",
                "GDDR6",
                "12GB",
                "300W",
                699,
                "https://images-na.ssl-images-amazon.com/images/I/61KS8t9N5eL._AC_SX679_.jpg"
            )
        )
        gpuList.add(
            GpuModelClass(
                "Gigabyte RTX 3070",
                "GDDR6",
                "8GB",
                "250W",
                499,
                "https://images-na.ssl-images-amazon.com/images/I/71U9eypS0sL._AC_SY450_.jpg"
            )
        )
        gpuList.add(
            GpuModelClass(
                "Aorus RTX 3060",
                "GDDR6",
                "12GB",
                "170W",
                329,
                "https://images.stockx.com/images/NVIDIA-GIGABYTE-AORUS-GeForce-RTX-3060-ELITE-12G-Graphics-Card-Graphics-Card.png?fit=fill&bg=FFFFFF&w=700&h=500&auto=format,compress&q=90&dpr=2&trim=color&updated_at=1614123121"
            )
        )
        gpuList.add(
            GpuModelClass(
                "Zotac Gaming RTX 3060 Ti",
                "GDDR6",
                "8GB",
                "200W",
                399,
                "https://www.vortez.net/news_file/20427_zotac-geforce-rtx-3060ti-twin-edge-box.jpg"
            )
        )
        gpuList.add(
            GpuModelClass(
                "Powercolor Radeon RX 6900 XT",
                "GDDR6",
                "16GB",
                "300W",
                999,
                "https://c1.neweggimages.com/ProductImageCompressAll1280/14-131-785-07.jpg"
            )
        )
        gpuList.add(
            GpuModelClass(
                "Msi Gaming Radeon RX 6800 XT",
                "GDDR6",
                "16GB",
                "300W",
                649,
                "https://images-na.ssl-images-amazon.com/images/I/81c3PiQLBUL._AC_SL1500_.jpg"
            )
        )
    }
}